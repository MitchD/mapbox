## 1. Mapbox-account maken

Als je dit nog niet gedaan hebt, maak op de website van Mapbox een account aan.

## 2. Nieuwe kaartstijl aanmaken in Mapbox Studio

Ga naar https://studio.mapbox.com. Klik op 'New Style'. Kies een basisstijl en klik daar op 'Create'.

Pas een paar dingen aan. Klik daarna rechtsboven op 'Publish'. Daarna staan in het tabblad 'Use' de access key en de style URL.

## 3. Maak een nieuw MVC project

Maak een nieuw project aan van het type 'ASP.NET Core Web Application'.

Het kan zijn dat er ook een NuGet package moet worden geïnstalleerd: Newtonsoft.JSON.

## 4. Kaart laden en configuratie aanpassen

Open `/Views/Shared/_Layout.cshtml` en plak deze code in de head:

```html
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.css' rel='stylesheet' />
```

Open `/Views/Home/Index.cshtml` en voeg dit onder @{ … }:

```html
@section Scripts {
    <script src="~/js/mapbox.js" asp-append-version="true"></script>
}
<div id='map' style='width: 100%; height: 600px;'></div>
```

Ga naar `/wwwroot/js` en voeg daar een nieuw item toe: mapbox.js


Plak hierin:
```js
mapboxgl.accessToken = '<access token>';
let map = new mapboxgl.Map({
    container: 'map',
    style: '<style url>',
});
```

Vervang de access token en style URL met de juiste uit Mapbox.

Voeg dit aan de opties toe om standaard op Amsterdam te starten:

```js
center: [4.893, 52.373],
zoom: 12,
maxZoom: 15,
```

## 5. Zoom-in / Zoom-out iconen toevoegen

```js
map.addControl(new mapboxgl.NavigationControl());
```

## 6. Controller aanmaken om de locatie GeoJSON in te laden

Haal het locatie GeoJSON bestand uit de repo en plaats deze in de root van je project
Linkermuisknop op het mapje controller -> Add new controller, noem deze MapboxController.

Plaats de volgende code om het JSON bestand uit te lezen:

```cs
[Route("mapbox")]
[ApiController]
public class MapboxController : ControllerBase {
    [HttpGet]
    public IActionResult GetAllData() {
        string jsonFileContents = System.IO.File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + @"\sharedRooms_geojson.json");
        object sharedRooms = JsonConvert.DeserializeObject(jsonFileContents);
        return Ok(sharedRooms);
    }
}
```

## 7. Markers toevoegen op de non-performance manier

Ga naar het mapbox.js bestand en fetch de JSON d.m.v de fetch API

```js
fetch('/mapbox')
.then(response => response.json())
.then(response => {
    response.features.forEach(marker => {
        let el = document.createElement('div');
        el.className = 'marker';

        let popup = new mapboxgl.Popup().setHTML(marker.properties.description);

        new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(popup)
            .addTo(map);
    });
}).then(err => {
    console.log(JSON.stringify(err));
});
```

Voeg in site.css het volgende toe om de markers zichtbaar te maken:

```css
.mapboxgl-marker.marker {
    width: 10px;
    height: 10px;
    background: #f0b7a1 linear-gradient(135deg, #f0b7a1 0%,#8c3310 50%,#752201 51%,#bf6e4e 100%);
    border-radius: 50%;
}
```
Dit moet binnen ```map.on('load', () => { ... });``` gezet worden.
Dit laadt heel langzaam, daarom gaan we ze nu op de juiste manier inladen.

## 8. Markers toevoegen op de juiste manier (met een laag)

Vervang de bovenstaande code met de onderstaande code:

```js
map.on('load', () => {
    map.addLayer({
        id: 'sharedRooms',
        type: 'circle',
        source: {
            type: 'geojson',
            data: '/mapbox'
        },
        paint: {
            'circle-color': '#446ccf'
        }
    });
});
```

Nu zijn de punten een heel stuk sneller, echter hebben we nog geen popups. Deze kunnen we toevoegen d.m.v de volgende code te gebruiken. Voeg deze binnen de onload toe:

```js
map.on('click', 'sharedRooms', (e) => {
    let coordinates = e.features[0].geometry.coordinates.slice();
    let description = e.features[0].properties.description;

    //while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        //coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    //}

    new mapboxgl.Popup().setLngLat(coordinates).setHTML(description).addTo(map);
});

// Change the cursor to a pointer when the mouse is over the places layer.
map.on('mouseenter', 'sharedRooms', () => {
    map.getCanvas().style.cursor = 'pointer';
});

// Change it back to a pointer when it leaves.
map.on('mouseleave', 'sharedRooms', () => {
    map.getCanvas().style.cursor = '';
});
```
Als je nu op een locatie op de map klikt zie je de popup verschijnen.

## 9. Polygon over een buurt tekenen
Kopieer het GeoJSON bestand uit de readme en plak deze in de root van je project.
Linkermuisknop op het mapje controller -> Add new controller, noem deze NeighborhoodsController.

Style deze zodat de controller er als volgd uitziet:

```cs
[Route("neighborhood")]
[ApiController]
public class NeighborhoodsController : ControllerBase {
    [HttpGet]
    public IActionResult GetAllData() {
        string jsonFileContents = System.IO.File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + @"\centrum-oost_geojson.json");
        object centrumOost = JsonConvert.DeserializeObject(jsonFileContents);
        return Ok(centrumOost);
    }
}
```

Voeg in mapbox.js onder de vorige layer het volgende toe:

```js
map.addLayer({
    'id': 'centrumOost',
    'type': 'fill',
    'source': {
        'type': 'geojson',
        'data': '/neighborhood'
    },
    'paint': {
        'fill-color': '#34495e',
        'fill-opacity': 0.3
    }
});
```

## 10. Popup bij polygon tonen

In site.css:

```css
.mapboxgl-popup {
    max-width: 400px;
    font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
}
```

in map.load functie:

```js
map.on('click', 'centrumOost', (e) => {
    new mapboxgl.Popup()
        .setLngLat(e.lngLat)
        .setHTML(e.features[0].properties.neighbourhood)
        .addTo(map);
});
```

## 11. Vlieg naar de buurt waar we de polygon overheen getekend hebben d.m.v het klikken op een knop

In HTML-document nieuw element toevoegen:

```html
<button id='fly'>Fly</button>
```

in CSS dit toevoegen

```css
#fly {
    width: 50%;
    height: 40px;
}
```

In JS dit toevoegen:

```js
document.getElementById('fly').addEventListener('click', () => {
    map.flyTo({
        center: [4.8704, 52.334],
        zoom: 15,
    });
});
```

